import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { sign } from 'fake-jwt-sign'; // For fakeAuthProvider only
import * as decode from 'jwt-decode';
import {
    BehaviorSubject,
    Observable,
    throwError as observableThrowError,
    of,
} from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Role } from './role.enum';
import { transformError } from '../common/common';

export interface AuthStatusInterface {
    isAuthenticated: boolean;
    userRole: Role;
    userId: string;
}

interface ServerAuthResponseInterface {
    accessToken: string;
}

export const defaultAuthStatus = {
    isAuthenticated: false,
    userRole: Role.None,
    userId: null,
};

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    private readonly authProvider: (
        email: string,
        password: string
    ) => Observable<ServerAuthResponseInterface>;

    public authStatus: BehaviorSubject<
        AuthStatusInterface
    > = new BehaviorSubject<AuthStatusInterface>(defaultAuthStatus);

    constructor() {
        // Fake login function to simulate roles
        this.authProvider = this.fakeAuthProvider;

        // Example of a real login call to server-side
        // this.authProvider = this.exampleAuthProvider
    }

    public login(
        email: string,
        password: string
    ): Observable<AuthStatusInterface | Error> {
        this.logout();

        const loginResponse = this.authProvider(email, password).pipe(
            map((value: ServerAuthResponseInterface) => {
                return decode(value.accessToken) as AuthStatusInterface;
            }),
            catchError(transformError)
        );

        loginResponse.subscribe(
            (res: AuthStatusInterface) => {
                this.authStatus.next(res);
            },
            (err: Error) => {
                this.logout();
                return observableThrowError(err);
            }
        );

        return loginResponse;
    }

    public logout(): void {
        this.authStatus.next(defaultAuthStatus);
    }

    private fakeAuthProvider(
        email: string,
        password: string
    ): Observable<ServerAuthResponseInterface> {
        if (!email.toLowerCase().endsWith('@test.com')) {
            return observableThrowError(
                'Failed to login! Email needs to end with @test.com.'
            );
        }

        const authStatus = {
            isAuthenticated: true,
            userId: 'e4d1bc2ab25c',
            userRole: email.toLowerCase().includes('cashier')
                ? Role.Cashier
                : email.toLowerCase().includes('clerk')
                ? Role.Clerk
                : email.toLowerCase().includes('manager')
                ? Role.Manager
                : Role.None,
        } as AuthStatusInterface;

        const authResponse = {
            accessToken: sign(authStatus, 'secret', {
                expiresIn: '1h',
                algorithm: 'none',
            }),
        } as ServerAuthResponseInterface;

        return of(authResponse);
    }
}
