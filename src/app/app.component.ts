import { Component, OnInit } from '@angular/core';
import { AuthService, AuthStatusInterface } from './auth/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
})
export class AppComponent implements OnInit {
    public displayAccountIcons: boolean = false;

    constructor(private authService: AuthService) {}

    public ngOnInit(): void {
        this.authService.authStatus.subscribe(
            (authStatus: AuthStatusInterface) =>
                (this.displayAccountIcons = authStatus.isAuthenticated)
        );
    }
}
