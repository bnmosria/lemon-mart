import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MaterialModule } from '../material.module';
import { InventoryRoutingModule } from './inventory-routing.module';

import { CategoriesComponent } from './categories/categories.component';
import { InventoryHomeComponent } from './inventory-home/inventory-home.component';
import { InventoryComponent } from './inventory.component';
import { ProductsComponent } from './products/products.component';
import { StockEntryComponent } from './stock-entry/stock-entry.component';

@NgModule({
    declarations: [
        CategoriesComponent,
        InventoryComponent,
        InventoryHomeComponent,
        ProductsComponent,
        StockEntryComponent,
    ],
    imports: [CommonModule, InventoryRoutingModule, MaterialModule],
})
export class InventoryModule {}
