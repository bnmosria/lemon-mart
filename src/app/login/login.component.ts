import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { AuthService, AuthStatusInterface } from '../auth/auth.service';
import { Role } from '../auth/role.enum';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less'],
})
export class LoginComponent implements OnInit {
    public loginForm: FormGroup;
    public loginError: string = '';
    public redirectUrl: string;
    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        route.paramMap.subscribe(
            (params: ParamMap) => (this.redirectUrl = params.get('redirectUrl'))
        );
    }

    public ngOnInit(): void {
        this.buildLoginForm();
    }

    public buildLoginForm(): void {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: [
                '',
                [
                    Validators.required,
                    Validators.minLength(8),
                    Validators.maxLength(50),
                ],
            ],
        });
    }

    public async login(submittedForm: FormGroup): Promise<void> {
        this.authService
            .login(submittedForm.value.email, submittedForm.value.password)
            .subscribe(
                (authStatus: AuthStatusInterface) => {
                    if (authStatus.isAuthenticated) {
                        this.router.navigate([
                            this.redirectUrl ||
                                this.homeRoutePerRole(authStatus.userRole),
                        ]);
                    }
                },
                (error: string) => (this.loginError = error)
            );
    }

    public homeRoutePerRole(role: Role): string {
        switch (role) {
            case Role.Cashier:
                return '/pos';
            case Role.Clerk:
                return '/inventory';
            case Role.Manager:
                return '/manager';
            default:
                return '/user/profile';
        }
    }
}
