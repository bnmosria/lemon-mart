import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { LemonIconComponent } from './components/lemon-icon/lemon-icon.component';

@NgModule({
    declarations: [LemonIconComponent],
    imports: [CommonModule, MaterialModule],
    exports: [LemonIconComponent],
})
export class IconsModule {}
