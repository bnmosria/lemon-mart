import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { MatIconModule } from '@angular/material';
import { LemonIconComponent } from './lemon-icon.component';

describe('LemonIconComponent', () => {
    let component: LemonIconComponent;
    let fixture: ComponentFixture<LemonIconComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [MatIconModule, HttpClientModule],
            declarations: [LemonIconComponent],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LemonIconComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
