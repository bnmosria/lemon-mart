import { Component, Input } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-lemon-icon',
    template: `
        <mat-icon svgIcon="lemon"></mat-icon>
        <ng-content></ng-content>
    `,
    styles: [],
})
export class LemonIconComponent {
    @Input() public iconStyle: string;

    constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
        iconRegistry.addSvgIcon(
            'lemon',
            sanitizer.bypassSecurityTrustResourceUrl(
                'assets/img/icons/lemon.svg'
            )
        );
    }
}
