import { Component, OnInit } from '@angular/core';
import { AuthService, AuthStatusInterface } from '../auth/auth.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less'],
})
export class HomeComponent implements OnInit {
    private _displayLogin: boolean = true;

    constructor(public authService: AuthService) {}

    public ngOnInit(): void {
        this.authService.authStatus.subscribe(
            (authStatus: AuthStatusInterface) =>
                (this._displayLogin = !authStatus.isAuthenticated)
        );
    }

    public get displayLogin(): boolean {
        return this._displayLogin;
    }
}
