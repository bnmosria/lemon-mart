import { async, TestBed } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { ObservableMedia } from '@angular/flex-layout';

import {
    commonTestingModules,
    commonTestingProviders,
    DomSanitizerFake,
    MatIconRegistryFake,
    ObservableMediaFake,
} from './common/common.testing';

import { AppComponent } from './app.component';
import { LemonIconComponent } from './icons/components/lemon-icon/lemon-icon.component';
import { LoginComponent } from './login/login.component';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: commonTestingModules,
            providers: commonTestingProviders.concat([
                { provide: ObservableMedia, useClass: ObservableMediaFake },
                { provide: MatIconRegistry, useClass: MatIconRegistryFake },
                { provide: DomSanitizer, useClass: DomSanitizerFake },
            ]),
            declarations: [AppComponent, LemonIconComponent, LoginComponent],
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
