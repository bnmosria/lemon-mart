import { browser, by, element } from 'protractor';

export class AppPage {
    public navigateTo() {
        return browser.get('/');
    }

    public getTitleText() {
        return element(by.css('app-home span.mat-display-2')).getText();
    }
}
